#include "brain.cpp"

#include <string>
#include <iostream>
using std::string;
using std::cout;
using std::endl;
using brain::Program;

int main () {

  const string expected = "hi";
  const string input = "hi";

  Program* program;
  string output;

  for (unsigned int i = 1; i < 8000; i ++) {

    program = new Program(i);

    bool success = program->run(output, input, 10);
    if (success) {
      cout << program->get_code() << endl;
      if (output == expected) {
        cout << output << endl;
        return 0;
      }
    }

    delete program;

  }

  return 1;

}
