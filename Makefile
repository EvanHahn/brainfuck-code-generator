default: program.cpp brain.h brain.cpp
	g++ -std=c++11 -Wall -o program.out program.cpp

run: default
	./program.out

clean:
	rm *.out
