#ifndef BRAINFUCK_H
#define BRAINFUCK_H

#include <string>
using std::string;

namespace brain {

  class Program {

    private:
      char* code;
      size_t length;

      bool is_valid_for (const string&);

    public:
      Program (const unsigned int);
      ~Program ();

      string get_code ();
      size_t get_length ();

      bool run (string&, const string&, const unsigned int);

  };

}

#endif
