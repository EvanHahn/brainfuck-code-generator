#include "brain.h"

#include <list>
#include <string>
#include <stdlib.h>
using std::string;
using std::list;

namespace brain {

  Program::Program (const unsigned int seed) {

    // first, figure out the program length

    {
      this->length = 0;
      unsigned int multiplier = 1;
      while (multiplier < seed) {
        this->length ++;
        multiplier *= 8;
      }
    }

    // next, figure out the program

    code = (char*) malloc(length);

    {
      unsigned int index;
      double multiplier = 1;
      for (int i = (this->length - 1); i >= 0; i -- ) {
        index = (unsigned int) (seed * (1.0 / multiplier)) % 8;
        this->code[i] = ("><+-.,[]")[index];
        multiplier *= 8;
      }
    }

  }

  Program::~Program () {
    free(this->code);
  }

  string Program::get_code () {
    return string(this->code, this->length);
  }

  size_t Program::get_length () {
    return this->length;
  }

  bool Program::is_valid_for (const string& input) {

    if (this->get_length() == 0) {
      return false;
    }

    int depth = 0;
    int remaining_inputs = input.size();
    bool found_print = false;

    bool first_character = true;

    for (unsigned int i = 0; i < this->length; i ++) {

      char character = this->code[i];

      if (first_character) {
        first_character = false;
        if (
          (character == '.') ||
          (character == '<') ||
          (character == '[') ||
          (character == ']')
        ) {
          return false;
        }
      } else {
        char previous = this->code[i - 1];
        if (
          ((character == '+') && (previous == '-')) ||
          ((character == '-') && (previous == '+')) ||
          ((character == '[') && (previous == ']')) ||
          ((character == '<') && (previous == '>')) ||
          ((character == '>') && (previous == '<')) ||
          ((character == '.') && (previous == '.'))
        ) {
          return false;
        }
      }

      if (character == ',') {
        remaining_inputs --;
        if (remaining_inputs < 0) {
          return false;
        }
      } else if (character == '.') {
        found_print = true;
      } else if (character == '[') {
        depth ++;
      } else if (character == ']') {
        depth --;
      }

      if (depth < 0) {
        return false;
      }

    }

    if (depth != 0) {
      return false;
    }

    return found_print;

  }

  bool Program::run (
    string& output,
    const string& input,
    const unsigned int max_instructions
  ) {

    if (!this->is_valid_for(input)) {
      return false;
    }

    list<int> tape(1, 0);
    unsigned int tape_size = 1;
    auto tape_index = tape.begin();
    unsigned int tape_index_int = 0;

    output.erase();

    unsigned int input_index = 0;
    const unsigned int input_size = input.size();
    unsigned int instruction_count = 0;

    for (unsigned int i = 0; i < this->length; i ++) {

      char character = code[i];

      if (character == '>') {
        if (tape_index_int == (tape_size - 1)) {
          tape.push_back(0);
          tape_size ++;
        }
        tape_index ++;
        tape_index_int ++;
      }

      else if (character == '<') {
        if (tape_index_int == 0) {
          return false;
        } else {
          tape_index --;
          tape_index_int --;
        }
      }

      else if (character == '+') {
        (*tape_index) ++;
      }

      else if (character == '-') {
        (*tape_index) --;
      }

      else if (character == '.') {
        output += (*tape_index);
      }

      else if (character == ',') {
        if (input_index == input_size) {
          return false;
        } else {
          (*tape_index) = input[input_index];
          input_index ++;
        }
      }

      else if (character == '[') {
        if ((*tape_index) == 0) {
          unsigned int depth = 1;
          while (depth != 0) {
            i ++;
            const char c = this->code[i];
            if (c == '[') {
              depth ++;
            } else if (c == ']') {
              depth --;
            }
          }
        }
      }

      else if (character == ']') {
        unsigned int depth = 1;
        while (depth != 0) {
          i --;
          const char c = this->code[i];
          if (c == '[') {
            depth --;
          } else if (c == ']') {
            depth ++;
          }
        }
        i --;
      }

      else {
        return false;
      }

      instruction_count ++; 
      if (instruction_count >= max_instructions) {
        return false;
      }

    }

    return true;

  }

}

